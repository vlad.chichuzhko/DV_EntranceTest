let b = document.getElementById("top_navigation_button");

b.onclick = function openMenu() {
    let x = document.getElementById("top_navigation");
    let b = document.getElementById("top_navigation_button");

    if(x.className === "topnav") {
        x.className += " open";
        b.text = "X";
    } else {
        x.className = "topnav";
        b.text = "☰";
    }
};

SELECT CONCAT(a.name, " ", a.surname) AS Actor FROM actor a
LEFT JOIN actor b ON a.surname = b.surname AND a.id <> b.id
WHERE b.id IS NULL
ORDER BY Actor

-- SELECT CONCAT(a.name, " ", a.surname) AS Actor FROM actor a
-- WHERE (SELECT COUNT(*) FROM actor a1 WHERE a1.surname = a.surname) < 2
-- ORDER BY Actor
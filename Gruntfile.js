module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compass: {
            dist: {
                options: {
                    sassDir: 'scss',
                    cssDir: 'css'
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css',
                    ext: '.min.css'
                }]
            }
        },
        // uglify: {
        //     my_target: {
        //         files: {
        //             'js/menu.min.js': 'js/menu.js',
        //             'js/slider.min.js': 'js/slider.js'
        //         }
        //     }
        // },
        watch: {
            scss: {
                files: 'scss/style.scss',
                tasks: ['compass']
            },
            css: {
                files: 'css/style.css',
                tasks: ['cssmin']
            },
            // js:{
            //     files: ['js/menu.js', 'js/slider.js'],
            //     tasks: ['uglify']
            // },
            core_files: {
                files: ['css/style.min.css', 'js/script.js', 'index.html'],
                options: {
                    livereload: true,
                },
            }
        },
    });
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['compass', 'cssmin']);
};
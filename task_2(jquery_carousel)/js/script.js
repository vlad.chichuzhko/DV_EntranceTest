;(function ($) {
    $.widget('vlad.slider', {
        options: {
            animationSpeed: 2000,
            width: 0
        },

        _create: function () {
            let $slidesWrapper = this.element.find('.slides'),
                $images = $slidesWrapper.find('> *'),
                firstSlide = $images.first().clone();

            if(this.options.width === 0) {
                this.options.width = $( window ).width();
            }

            $slidesWrapper.css({
                left: (-this.options.width) + 'px'
            });

            $slidesWrapper.prepend($images.last().clone());
            $slidesWrapper.append(firstSlide);
            $images = $slidesWrapper.find('> *');

            for (let i = 0; i < $images.length; i++){
                $images.eq(i).css({
                    left: (i*this.options.width) + 'px'
                })
            }

            this.element.find('.previous, .next').click(this.move.bind(this));
        },

        move: function (event) {
            let direction = $(event.currentTarget).hasClass('next') ? 1 : -1,
                duration = this.options.animationSpeed,
                $slides = this.element.find('.slides'),
                $images = $slides.find('> *'),
                $navigationButtons = this.element.find('.previous, .next'),
                sliderCurrentPosition = parseInt($slides.position().left),
                numberOfSlides = $images.length - 2,
                sliderWidth = this.options.width,
                currentSlideIndex = -(left + sliderWidth) / sliderWidth + 1,  //1
                cycle = false;

            $navigationButtons.addClass('disabled');

            currentSlideIndex += direction;
            cycle = (currentSlideIndex === 0 || currentSlideIndex > numberOfSlides);

            $slides.css({
                transition: 'all ' + duration + 'ms',
                left: (sliderCurrentPosition - direction * sliderWidth) + 'px'
            });

            setTimeout(function () {
                if (cycle) {
                    currentSlideIndex = (currentSlideIndex === 0) ? numberOfSlides : 1;
                    $slides.css({
                        left: -sliderWidth * currentSlideIndex,
                        transitionDuration: '0ms'
                    })
                }
                $navigationButtons.removeClass('disabled');
            }, duration);


                // $slides.animate({left: (left - direction * width) + 'px'}, duration, function () {
                //
                //     current += direction;
                //
                //     cycle = (current === 0 || current > length);
                //
                //     if (cycle) {
                //         current = (current === 0) ? length : 1;
                //         $slides.css({left: -width * current});
                //     }
                // });
            // }


            // slider.style.transitionDuration = animationSpeed + 'ms';


            // only for separate images
            // for (i = 0; i < images.length; i++) {
            //     if(parseInt(images[images.length - 1].offsetLeft) === 0 && direction > 0){
            //         for (j = 0; j < images.length; j++) {
            //             images[j].style.left = (j*width) + 'px';
            //         }
            //         break;
            //     } if(parseInt(images[0].offsetLeft) === 0 && direction < 0){
            //         for (j = 0; j < images.length; j++) {
            //             images[j].style.left = (direction*(images.length - j - 1)*width) + 'px';
            //         }
            //         break;
            //     } else {
            //         let left =  parseInt(images[i].offsetLeft);
            //         images[i].style.left = (left - direction*width) + 'px';
            //     }
            //     console.log(width);
            // }
        }
    });



//     Slider.prototype._create = function (options) {
// debugger;
//         let images = document.getElementsByClassName('img');
//         let slider = document.getElementsByClassName('slider')[0];
//         slider.style.left = (-options.width) + 'px';
//         slider.style.transition = 'all ' + options.animationSpeed + 'ms';
//         slider.insertBefore(images[images.length - 1].cloneNode(true), images[0]);
//         slider.appendChild(images[1].cloneNode(true));
//         images = document.getElementsByClassName('img');
//         // images[0].style.zIndex = -2;
//         // images[images.length - 1].style.zIndex = -2;
//         // slider.style.transition = 'all ' + options.animationSpeed + 'ms';
//         // images[images.length - 2].style.display = 'none';
//         // images[images.length - 1].style.display = 'none';
//
//         document.getElementsByClassName('next')[0].onclick = function () {
//             this.move(1, options.width, options.animationSpeed);
//         }.bind(this);
//
//         document.getElementsByClassName('prev')[0].onclick = function () {
//             this.move(-1, options.width, options.animationSpeed);
//         }.bind(this);
//     };

})(jQuery);

//---------------------------------------------------------

// $(document).ready(function(){
//     let width = $( window ).width(),
//     slider = $('.slider'),
//     images = $('.img'),
//     length = images.length,
//     current = 1,
//     first = images.filter(':first'),
//     last = images.filter(':last'),
//     buttons = $('.fas');
//
//     first.before(last.clone(true));
//     last.after(first.clone(true));
//     slider.css({left: -width});
//
//     buttons.on('click', function() {
//
//         let cycle, step;
//
//         if (slider.is(':not(:animated)')) {
//
//             cycle = false;
//             step = (this.id === 'next') ? 1 : -1;
//
//             slider.animate({left: '+=' + (-width * step)}, function () {
//
//                 current += step;
//
//                 cycle = (current === 0 || current > length);
//
//                 if (cycle) {
//                     current = (current === 0) ? length : 1;
//                     slider.css({left: -width * current});
//                 }
//             });
//         }
//     })
//     // https://stackoverflow.com/a/15877302
// });

//------------------------------------------------------

;(function ($) {
    $.widget('vlad.carousel', {
        options: {
            animationSpeed: 2000
        },

        _create: function () {
            this.element.find('.item').click(function () {
                this.moveStar();
            }.bind(this));
        },

        moveStar: function () { //heptagonal
            let duration = this.options.animationSpeed,
                step = 3,
                $items = this.element.find('.item');

            for(let i = 0; i < $items.length; i++){
                let nextIndex = i + step;
                if (nextIndex > 6) {
                    nextIndex -= 7;
                }
                // if (nextIndex < 0) { //for reverse direction
                //     nextIndex += 7;
                // }

                let nextTop = $items.eq(nextIndex).css('top'),
                    nextLeft = $items.eq(nextIndex).css('left');

                console.log(i + ' -> ' + nextIndex);

                $items.eq(i).css({
                    top: nextTop,
                    left: nextLeft,
                    transition: 'all ' + duration + 'ms'
                });
                $items.eq(i).addClass('disabled');

                setTimeout(function(){
                    $items.eq(i).removeClass('disabled');
                }, duration)
            }
        }
    })
})(jQuery);

// $(document).ready(function () {
//     $('.item').click(function () {
//         if ($(this).is(':animated')) {
//             return false;
//         }
//         moveStar();
//     });
//
//     function moveStar() { //heptagonal
//         let step = 3;
//         $('.item').each(function () {
//             let nextIndex = $(this).index() + step;
//             if (nextIndex > 6) {
//                 nextIndex -= 7;
//             }
//             if (nextIndex < 0) {
//                 nextIndex += 7;
//             }
//             console.log($(this).index() + ' -> ' + nextIndex);
//
//             $(this).animate($('.item').eq(nextIndex).position(), 3000);
//         });
//     }
// });

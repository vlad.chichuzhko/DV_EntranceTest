SELECT
  s.name AS Studio,
  CONCAT(a.name, " ", a.surname) AS Actor,
  COUNT(DISTINCT f.id) AS "Number of Films",
  COUNT(fe.id) AS "Number of Fees",
  SUM(COALESCE(fe.fee, 0)) AS "Sum of Fees",
  ROUND(AVG(COALESCE(fe.fee, 0)), 0) AS "Average Fee"
FROM studio AS s
  INNER JOIN film AS f ON f.studio_id = s.id
  INNER JOIN film_actor AS fa ON fa.film_id = f.id
  INNER JOIN actor AS a ON a.id = fa.actor_id
  INNER JOIN fee AS fe ON fe.film_actor_id = fa.id
WHERE (YEAR(CURDATE()) - f.year) <= 10
GROUP BY s.id, a.id
ORDER BY AVG(fe.fee) DESC
